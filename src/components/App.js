import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import {
  TextField,
  Typography,
  Container
} from '@material-ui/core';
import {
  Table, TableHead, TableBody, TableRow, TableCell
} from '@material-ui/core';
import data from '../data/coronaData.json';
import { capitalizeString } from '../helper/app';
import { LEVEL, LEVEL_TYPES } from '../helper/constants';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: '10px',
    backgroundColor: '#cfe8fc',
    height: '75vh'
  },
  tableWrapper: {
    backgroundColor:' #fff',
    maxHeight: 600,
    overflow: 'auto',
  },
  table: {
    minWidth: '80%',
  },
}));

const App = () => {
  const classes = useStyles();
  const [state, setState] = React.useState({
    selChoice: []
  });

  const setOptions = () => {
    const options = [];
    try {
      const { selChoice } = state;
      if (selChoice.length === LEVEL.ONE) {
        const fData = data[selChoice[0].id];
        for (const ct in fData) {
          options.push({ id: ct, title: capitalizeString(ct)});
        }
      } else if (selChoice.length === LEVEL.TWO) {
        const fData = data[selChoice[0].id][selChoice[LEVEL.ONE].id];
        for (const ct in fData) {
          options.push({ id: ct, title: capitalizeString(ct)});
        }
      } else {
        if (!selChoice.length) {
          for (const ct in data) {
            options.push({ id: ct, title: capitalizeString(ct)});
          }
        }
      }
      return options;
    } catch(err) {
      return options;
    }
  };

  const getDataTemplate = () => {
    const result = [];
    try {
      const { selChoice } = state;
      if (selChoice.length === LEVEL.THREE && selChoice[0].id === LEVEL_TYPES.INDIA) {
        const fData = data[LEVEL_TYPES.INDIA][selChoice[LEVEL.ONE].id]['list'];
        fData.forEach((item) => {
          result.push(
            <TableRow key={item.area}>
              <TableCell>{item.area || ''}</TableCell>
              <TableCell align="right">{item.case || ''}</TableCell>
              <TableCell align="right">{item.death || '-'}</TableCell>
            </TableRow>
          )
        });
        return (<Table className={classes.table} stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell>Location</TableCell>
              <TableCell align="right">Active cases</TableCell>
              <TableCell align="right">Death</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {result}
          </TableBody>
        </Table>);
      } else if (selChoice.length === LEVEL.TWO && selChoice[0].id === LEVEL_TYPES.WORLD) {
        const fData = data[LEVEL_TYPES.WORLD]['list'];
        fData.forEach((item) => {
          result.push(
            <TableRow key={item.area}>
              <TableCell>{item.area || ''}</TableCell>
              <TableCell align="right">{item.case || ''}</TableCell>
              <TableCell align="right">{item.death || ''}</TableCell>
            </TableRow>
          )
        });
        return (<Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Location</TableCell>
              <TableCell align="right">Active cases</TableCell>
              <TableCell align="right">Death</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {result}
          </TableBody>
        </Table>);
      }
      return null;
    } catch(err) {
      return null;
    }
    
  };

  return (
    <React.Fragment>
      <Container maxWidth="sm" className={classes.root}>
        <Typography component="div" style={{ padding: '20px'}}>
          <Autocomplete
            multiple
            autoHighlight
            id="tags-standard"
            options={setOptions()}
            getOptionLabel={(option) => option.title}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Select your choice to view"
              />
            )}
            onChange={(e, value) => {
              if(value && value.length) {
                setState((s) => ({ ...s, selChoice: value}));
              } else {
                setState((s) => ({ ...s, selChoice: []}));
              }
            }}
          />
        </Typography>
          <div className={classes.tableWrapper}>
            {getDataTemplate()}
          </div>
      </Container>
    </React.Fragment>
  );
}

export default App;