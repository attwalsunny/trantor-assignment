export const capitalizeString = (str = '') => str.toUpperCase();
export const normalizeNumber = (num = 0) => parseInt(num, 10);